FROM fabric8/java-centos-openjdk8-jdk:1.5.1

MAINTAINER rhuss@redhat.com

EXPOSE 8080

ENV TOMCAT_VERSION 9.0.60
ENV DEPLOY_DIR /maven

USER root

RUN yum -y update
RUN yum -y install sudo \
    tar \
    gzip \
    openssh-clients \
    java-11-openjdk-devel\
    vi \
    find

RUN groupadd tomcat
RUN useradd pipelinedeploy
RUN usermod -a -G tomcat  pipelinedeploy


# Get and Unpack Tomcat
RUN curl http://archive.apache.org/dist/tomcat/tomcat-9/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz -o /tmp/catalina.tar.gz \
 && tar xzf /tmp/catalina.tar.gz -C /opt \
 && ln -s /opt/apache-tomcat-${TOMCAT_VERSION} /opt/tomcat \
 && chown -R jboss /opt/tomcat /opt/apache-tomcat-${TOMCAT_VERSION} \
 && rm /tmp/catalina.tar.gz

# Add roles
ADD tomcat-users.xml /opt/apache-tomcat-${TOMCAT_VERSION}/conf/

# Startup script
ADD deploy-and-run.sh /opt/apache-tomcat-${TOMCAT_VERSION}/bin/

RUN chmod 755 /opt/apache-tomcat-${TOMCAT_VERSION}/bin/deploy-and-run.sh \
 && rm -rf /opt/tomcat/webapps/examples /opt/tomcat/webapps/docs  \
 && chmod go+rx /opt/tomcat/conf /opt/apache-tomcat-${TOMCAT_VERSION}/conf \
 && chmod -R go+r /opt/tomcat/conf /opt/apache-tomcat-${TOMCAT_VERSION}/conf \
 && chgrp -R 0 /opt/tomcat/webapps \
 && chmod -R g=u /opt/tomcat/webapps

VOLUME [ "/opt/tomcat/logs", "/opt/tomcat/work", "/opt/tomcat/temp", "/tmp/hsperfdata_root" ]

ENV CATALINA_HOME /opt/tomcat
ENV PATH $PATH:$CATALINA_HOME/bin

CMD /opt/tomcat/bin/deploy-and-run.sh
RUN sed -i "19d" /opt/tomcat/webapps/manager/META-INF/context.xml
RUN sed -i "20d" /opt/tomcat/webapps/manager/META-INF/context.xml
USER jboss
